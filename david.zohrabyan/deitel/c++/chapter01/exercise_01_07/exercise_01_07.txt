Question.
    Why is so much attention today focused on object-oriented programming in general and C++ in particular?

Answer.
    Actually, object technology is a packaging scheme that helps us create meaningful software units. These can be large
    and are highly focussed on particular applications areas. There are date objects, time objects, paycheck objects, invoice objects, audio objects, video objects, file
    objects, record objects and so on. In fact, almost any noun can be reasonably represented as an object.


    We live in a world of objects. Just look around you. There are cars, planes, people, animals, buildings, traffic lights, elevators and the like. Before object-oriented
    languages appeared, programming languages (such as FORTRAN, COBOL, Pascal, Basic and C) were focussed on actions (verbs) rather than on things or objects
    (nouns). Programmers living in a world of objects programmed primarily using verbs. This made it awkward to write programs. Now, with the availability of popular
    object-oriented languages such as C++ and Java, programmers continue to live in an object-oriented world and can program in an object-oriented manner. This is
    a more natural process than procedural programming and has resulted in significant productivity enhancem ents.


P.S.
    Sorry it was copied from the book. It`s describes better than my onwn explanation.
