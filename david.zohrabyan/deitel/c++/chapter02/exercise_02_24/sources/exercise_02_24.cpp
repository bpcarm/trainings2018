#include <iostream>

int
main()
{
    int number;
    
    std::cout << "Enter integer: ";
    std::cin >> number;

    if (0 == number % 2) {
        std::cout << "Integer " << number << " is even" << std::endl;
        return 0;
    }

    std::cout << "Integer " << number << " is odd" << std::endl;

    return 0;
}

