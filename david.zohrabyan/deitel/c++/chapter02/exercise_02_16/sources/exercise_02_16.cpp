#include <iostream>

int
main() 
{
    int number1;
    int number2;
    
    std::cout << "Enter two numbers: ";
    std::cin >> number1 >> number2;

    std::cout << number1  << " + " << number2 << " = " << number1 + number2 << "\n";
    std::cout << number1  << " * " << number2 << " = " << number1 * number2 << "\n";
    std::cout << number1  << " - " << number2 << " = " << number1 - number2 << "\n";

    if (0 == number2) {
        std::cout << "Error 1: A number can't be divided to zero." << std::endl;
        return 1;
    } 
    
    std::cout << number1 <<  " / " <<  number2 << " = " << number1 / number2 << std::endl;
    
    
    return 0;
}

