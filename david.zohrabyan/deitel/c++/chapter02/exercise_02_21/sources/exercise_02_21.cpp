#include <iostream>

int
main()
{
    std::cout << "*********     ***       *        *     \n";
    std::cout << "*       *   *     *    ***      * *    \n";
    std::cout << "*       *  *       *  *****    *   *   \n";
    std::cout << "*       *  *       *    *     *     *  \n";
    std::cout << "*       *  *       *    *    *       * \n";
    std::cout << "*       *  *       *    *     *     *  \n";
    std::cout << "*       *  *       *    *      *   *   \n";
    std::cout << "*       *   *     *     *       * *    \n";
    std::cout << "*********     ***       *        *     " << std::endl;

    return 0;
}
