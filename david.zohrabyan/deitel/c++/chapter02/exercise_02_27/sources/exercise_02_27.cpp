#include <iostream>

int
main()
{
    std::cout << "The character 'A'       has the value " << static_cast<int>('A') << std::endl;
    std::cout << "The character 'a'       has the value " << static_cast<int>('a') << std::endl;
    std::cout << "The character 'B'       has the value " << static_cast<int>('B') << std::endl;
    std::cout << "The character 'b'       has the value " << static_cast<int>('b') << std::endl;
    std::cout << "The character 'C'       has the value " << static_cast<int>('C') << std::endl;
    std::cout << "The character 'c'       has the value " << static_cast<int>('c') << std::endl;
    std::cout << "The character 'D'       has the value " << static_cast<int>('D') << std::endl;
    std::cout << "The character 'd'       has the value " << static_cast<int>('d') << std::endl;
    std::cout << "The character '12'      has the value " << static_cast<int>(12) << std::endl;
    std::cout << "The character '-12'     has the value " << static_cast<int>(-12) << std::endl;
    std::cout << "The character '$'       has the value " << static_cast<int>('$') << std::endl;
    std::cout << "The character '#'       has the value " << static_cast<int>('#') << std::endl;
    std::cout << "The character '@'       has the value " << static_cast<int>('@') << std::endl;
    std::cout << "The character '?'       has the value " << static_cast<int>('?') << std::endl;
    std::cout << "The character 'space'   has the value " << static_cast<int>(' ') << std::endl;

    return 0;
}
