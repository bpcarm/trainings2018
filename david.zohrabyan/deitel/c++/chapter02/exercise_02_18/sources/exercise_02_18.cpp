#include <iostream>

int
main()
{
    int number1, number2;
    
    std::cout << "Enter two numbers: ";
    std::cin >> number1 >> number2;
    std::cout << std::endl;

    if (number1 > number2) {
        std::cout << number1 << " is larger." << std::endl;
        return 0;
    }

    if (number2 > number1) {
        std::cout << number2 << " is larger." << std::endl;
        return 0;
    }
    
    std::cout << "These numbers are equal." << std::endl;
    

    return 0;
}
