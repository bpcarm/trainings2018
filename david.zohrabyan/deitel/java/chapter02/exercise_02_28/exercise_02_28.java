import java.util.Scanner;

public class exercise_02_28 {
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);

        float radius;

        System.out.print("Enter the radius: ");
        radius = input.nextFloat();
            
        if (radius <= 0) {
            System.out.println("Error1. The raduius of circle can`t be zero or less.Please provide at least one positive number to calculate");
            System.exit(0);
        }

        System.out.printf("%nThe diameter of the circle is %f%n", 2 * radius);
        System.out.printf("The circumference of the circle is %f%n", 2 * 3.14159 * radius);
        System.out.printf("The are of the circle is %f%n", 3.14159 * radius * radius);
    }
}

