import java.util.Scanner;

public class exercise_02_33 {
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);

        String bmiOfficalValues = "BMI VALUES \n"+
                                  "Underweight: less than 18.5\n"+
                                  "Normal:      between 18.5 and 24.9\n"+
                                  "Overweight:  between 25 and 29.9\n"+
                                  "Obese:       30 or greater\n";
    
        System.out.print(bmiOfficalValues);

        System.out.println("Body Mass Index Calculator\n");

        System.out.print("Enter your height in kilograms: ");
        int weightInKilograms = input.nextInt();

        if (weightInKilograms <= 0) {
            System.out.println("Enter 1: Human weight cannot be zero or less");
            System.exit(1);
        }

        System.out.print("Enter you height in meters: ");
        int heightInMeters = input.nextInt();

        if (heightInMeters <= 0) {
            System.out.println("Enter 2: Human height cannot be zero or less");
            System.exit(2);
        }

        int bmi = weightInKilograms / (heightInMeters * heightInMeters);

        System.out.printf("Your BMI: %d\n", bmi);

    }
}

