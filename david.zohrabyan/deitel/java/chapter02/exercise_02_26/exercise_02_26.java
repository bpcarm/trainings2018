import java.util.Scanner;

public class exercise_02_26 {
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);

        System.out.print("Two integers: ");
        int number1 = input.nextInt();
        int number2 = input.nextInt();

        if (0 == number2) {
            System.out.println("Error 1: Second number can`t be zero.");
            System.exit(1);
        }

        if (0 == number1 % number2) {
            System.out.printf("%n%d is a multiple of %d%n", number1, number2);
            System.exit(0);
        }
        
        System.out.printf("%n%d is not a multiple of %d%n", number1, number2);
        
    }
}
