import java.util.Scanner;

public class exercise_02_17 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int number1;
        int number2;
        int number3;

        System.out.print("Enter three integers: ");

        number1 = input.nextInt();
        number2 = input.nextInt();
        number3 = input.nextInt();
        
        int sum;
        sum = number1 + number2 + number3; 
        System.out.printf("%n%d + %d + %d = %d%n", number1, number2, number3, sum);

        int average;
        average = (number1 + number2 + number3) / 3;
        System.out.printf("(%d + %d + %d) / 3 = %d%n ", number1, number2, number3, average); 

        int product;
        product = number1 * number2 * number3;
        System.out.printf("%d * %d * %d = %d%n", number1, number2, number3, product);


        int max = number1;
        if (number2 > max) {
            max = number2;
        }
        if (number3 > max) {
            max = number3;
        }

        int min = number1;
        if (number2 < min) {
            min = number2;
        }
        if (number3 < min) {
            min = number3;
        }

        System.out.printf("Smallest number is %d%n", min);
        System.out.printf("Largerst number is %d%n", max);

    }
}

