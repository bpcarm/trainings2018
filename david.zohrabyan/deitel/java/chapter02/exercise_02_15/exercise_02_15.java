import java.util.Scanner;

public class exercise_02_15 {
    public static void main(String [] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter first integer: ");
        int number1 = input.nextInt();

        System.out.print("Enter second integer: ");
        int number2 = input.nextInt();

        System.out.printf("%n%d + %d = %d%n", number1, number2, number1 + number2);
        System.out.printf("%d - %d = %d%n", number1, number2, number1 - number2);
        System.out.printf("%d * %d = %d%n", number1, number2, number1 * number2);

        if (0 == number2) {
            System.out.println("Error 1: A number can't be divided to zero.");
            System.exit(1);
        }
        

        System.out.printf("%d / %d = %d%n", number1, number2, number1 / number2);
    }
}
