import java.util.Scanner;

public class exercise_02_16 {
    public static void main(String [] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter two integers: ");
        int number1 = input.nextInt();
        int number2 = input.nextInt();

        if (number1 == number2) {
            System.out.println("These numbers are equal");
            System.exit(0);
        }

        int max = number1;
        if (number2 > number1) {
            max = number2;
        }
        
        System.out.printf("%d is largest%n", max);
    }
}

