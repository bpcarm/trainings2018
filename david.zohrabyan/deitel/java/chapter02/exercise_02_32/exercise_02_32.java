import java.util.Scanner;

public class exercise_02_32 {
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);

        int negativeNumbers = 0, ZeroValuesNumbers = 0, positiveNumbers = 5;

        System.out.print("Enter five integers: ");
        int number1 = input.nextInt();
        int number2 = input.nextInt();
        int number3 = input.nextInt();
        int number4 = input.nextInt();
        int number5 = input.nextInt();
        
        if (number1 < 0) {
            negativeNumbers = negativeNumbers + 1;
        }
        
        if (0 == number1) {
            ZeroValuesNumbers = ZeroValuesNumbers + 1;
        }
        
        if (number2 < 0) {
            negativeNumbers = negativeNumbers + 1;
        }
        
        if (0 == number2) {
            ZeroValuesNumbers = ZeroValuesNumbers + 1;
        }

        if (number3 < 0) {
            negativeNumbers = negativeNumbers + 1;
        }
        
        if (0 == number3) {
            ZeroValuesNumbers = ZeroValuesNumbers + 1;
        }

        if (number4 < 0) {
            negativeNumbers = negativeNumbers + 1;
        }
        
        if (0 == number4) {
            ZeroValuesNumbers = ZeroValuesNumbers + 1;
        }

        if (number5 < 0) {
            negativeNumbers = negativeNumbers + 1;
        }
        
        if (0 == number5) {
            ZeroValuesNumbers = ZeroValuesNumbers + 1;
        }

        positiveNumbers = positiveNumbers - negativeNumbers - ZeroValuesNumbers;

        System.out.printf("%nPositive numbers: %d %nNegative numbers: %d %nZero Values numbers: %d%n", positiveNumbers, negativeNumbers, ZeroValuesNumbers);
    }
}
