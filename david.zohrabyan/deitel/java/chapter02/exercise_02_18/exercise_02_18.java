public class exercise_02_18 {
    public static void main(String[] args) {
        
        String line = "*********     ***       *       *     \n"+
                      "*       *   *     *    ***     * *    \n"+
                      "*       *  *       *  *****   *   *   \n"+
                      "*       *  *       *    *    *     *  \n"+
                      "*       *  *       *    *   *       * \n"+
                      "*       *  *       *    *    *     *  \n"+
                      "*       *  *       *    *     *   *   \n"+
                      "*       *   *     *     *      * *    \n"+
                      "*********     ***       *       *     \n";

        System.out.print(line);
    }
}
