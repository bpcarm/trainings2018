import java.util.Scanner;

public class exercise_02_25 {
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);

        System.out.print("Enter integer: ");
        int integer = input.nextInt();

        if (0 == integer % 2) {
            System.out.printf("%n%d is an even integer%n", integer);
            System.exit(0);
        } 
        
        System.out.printf("%n%d is an odd integer%n", integer);
        
    }
}
