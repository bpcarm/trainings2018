import java.util.Scanner;

public class exercise_02_30 {
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);

        System.out.print("Enter five digit number: ");
        int number = input.nextInt();
        
        if (99999 < number) {
            System.out.println("Error1: Input value exceeded five digit number.");
            System.exit(1);
        }

        if (10000 > number) {
            System.out.println("Error2: Input value less than five digit number.");
            System.exit(2);
        }

        int digit1 = (number / 10000) % 10;
        int digit2 = (number / 1000) % 10;
        int digit3 = (number / 100) % 10;
        int digit4 = (number / 10) % 10;
        int digit5 = (number / 1) % 10;

        System.out.printf("%n%d   %d   %d   %d   %d%n", digit1, digit2, digit3, digit4, digit5);
    }
}
