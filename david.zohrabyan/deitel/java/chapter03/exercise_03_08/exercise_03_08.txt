Question. Most classes need to be imported before they can be used in an app. Why is every app allowed to use classes System and String without first importing them?


Answer.
    System and String belong to java.lang package package which is imported by default and an always be used unqualified.
