Procedure to be followed:
    
    1. Store the gender-specific words and their corresponding neutral replacements like a dictionary.

    2. Begin reading the given paragraph, one word at a time.

    3. If the word just read in occurs in the replacement dictionary, replace it with the neutral word from the dictionary and send it to the output.
       Otherwise copy the word to the output without any change.

    4. If the paragraph has ended, exit. Otherwise read in the text word and repeat Step 3 with it.
