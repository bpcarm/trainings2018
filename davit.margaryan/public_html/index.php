<?php
///////////////////////////////////////////////////////////////////
// 
// VERY SIMPLE MVC ENGINE C++ Institute © 2016
//
// Main file of engine. All process is ruled from here
// by configuring and bootstrapping.
// 
///////////////////////////////////////////////////////////////////
require_once 'config/autoload.php';
Libs\Bootstrap::process();
?>
