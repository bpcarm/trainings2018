#include <iostream>

int
main()
{
    int number1, number2, number3;
    std::cout << "Please enter three numbers: ";
    std::cin >> number1 >> number2 >> number3;
    
    int max = number1;
    if (number2 > max) {
        max = number2;
    }
    if (number3 > max) {
        max = number3;
    }
    int min = number1;
    if (number2 < min) {
        min = number2;
    }
    if (number3 < min) {
        min = number3;
    }

    std::cout << "Sum = " << (number1 + number2 + number3) << std::endl;
    std::cout << "Average = " << (number1 + number2 + number3) / 3 << std::endl;
    std::cout << "Multiplication = " << (number1 * number2 * number3) << std::endl;
    std::cout << "Smalest = " << min << std::endl;
    std::cout << "Greatest = " << max << std::endl;
    return 0;
}
