#include <iostream>

int
main ()
{

    int number, digit1, digit2, digit3, digit4, digit5;
    
    std::cout << "Enter five-digit number: ";
    std::cin >> number;
    
    if (number >= 100000) {
        std::cout << "ERROR your figure not five-digit. Please start the program once again and enter five-digit \n ";
        return 0;
    }
    if (number <= 9999) {
        std::cout << "ERROR your figure not five-digit. Please start the program once again and enter five-digit \n ";
        return 0;
    }

    digit1 = number / 10000;
    digit2 = (number / 1000) % 10;
    digit3 = (number / 100) % 10;
    digit4 = (number / 10) % 10;
    digit5 = number % 10;

    std::cout << digit1 << " "<< digit2 << " " << digit3 << " " << digit4 << " " << digit5 << " " << std::endl;

    return 1;
}
