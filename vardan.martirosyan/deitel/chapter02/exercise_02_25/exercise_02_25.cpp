#include <iostream>

int 
main()
{
    int number1, number2;

    std::cout << "Enter first variable: " << std::endl;
    std::cin  >> number1 ;

    std::cout << "Enter second variable: " << std::endl;
    std::cin  >> number2;

    if (number2 == 0) {
        std::cout << "ERROR" << std::endl;
            return 0;
    }
    if (number1 % number2 == 0)
        std::cout << "The first is multiple to the second: " << std::endl;

    if (number1 % number2 != 0)
        std::cout << "The first isn`t multiple to the second: " << std::endl;

    return 0;
}

