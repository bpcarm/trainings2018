#ifndef __SLIST_HPP__
#define __SLIST_HPP__

template<typename T>
class slist
{
private:
    struct node {
        node(const T& value, node* next = 0) : value_(value), next_(next) {}
        T value_;
        node* next_;
    };

public:
    class const_iterator
    {
    public:
        const_iterator(const node* p) : p_(p) {}
        bool operator==(const const_iterator& rhv) { return p_ == rhv.p_; }
        bool operator!=(const const_iterator& rhv) { return p_ != rhv.p_; }
        const T& operator*() const { return p_->value_; }
        const_iterator& operator++() { p_ = p_->next_; }
    private:
        const node* p_;
    };

public:
    slist() : root_(0), size_(0) {}

    bool isEmpty() const { return 0 == size_; }
    unsigned int size() const { return size_; }
    const_iterator begin() const { return const_iterator(root_); }
    const_iterator end() const { return const_iterator(0); }

    void push_front(const T& value);

private:
    node* root_;
    unsigned int size_;
};

template <typename T>
void
slist<T>::push_front(const T& value)
{
    if (isEmpty()) {
        root_ = new node(value);
    } else {
        root_ = new node(value, root_);
    }
    ++size_;
}

#endif /// __SLIST_HPP__

