#ifndef __CHAT_CONNECTION__
#define __CHAT_CONNECTION__

#include <thread>

class ChatServer;

class ChatConnection
{
public:
    ChatConnection();
    ChatConnection(ChatServer* server, const int clientSocket);
    int connect();
    int send(const char* message, const int messageSize);
private:
    void execute();
    void receive();
private:
    std::thread thread_;
    ChatServer* server_;
    int mainSocket_;
};

#endif /// __CHAT_CONNECTION__

