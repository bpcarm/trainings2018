#include "headers/core/ChatClient.hpp"
#include "headers/core/ChatConnection.hpp"

#include <iostream>

int
ChatClient::execute()
{
    ChatConnection connection;
    const int connResult = connection.connect();
    if (connResult != 0) {
        return connResult;
    }
    return 0;
}

