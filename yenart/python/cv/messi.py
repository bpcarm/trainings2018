import cv2;
import matplotlib.pyplot as plt;

messi = "messi5.jpg";
img = cv2.imread(messi, 0);
if type(img) == type(None):
    print('Error 1: Cannot open file %s'%messi);
    exit(1);

plt.imshow(img, cmap = "gray", interpolation = "bicubic");
plt.xticks([]);
plt.yticks([]);
plt.show();
#cv2.waitKey(0);
#cv2.destroyAllWindows();

#cv2.imwrite("messi5gray.jpg", img);

