#include <iostream>

int
main()
{
    int number1, number2, number3, number4, number5, max, min;

    std::cout << "Enter five integers: ";
    std::cin >> number1 >> number2 >> number3 >> number4 >> number5;

    max = number1;
    if (number2 > max) {
        max = number2;
    }
    if (number3 > max) {
        max = number3;
    }
    if (number4 > max) {
        max = number4;
    }
    if (number5 > max) {
        max = number5;
    }
    std::cout << "The max is " << max << std::endl;
    
    min = number1;
    if (number2 < min) {
        min = number2;
    }
    if (number3 < min) {
        min = number3;
    }
    if (number4 < min) {
        min = number4;
    }
    if (number5 < min) {
        min = number5;
    }
    std::cout << "The min is " << min << std::endl;

    return 0;
} 
