#include <string>

class GradeBook
{
public:
    GradeBook(std::string name, std::string instructor);
    void setCourseName(std::string name);
    std::string getCourseName();
    void setInstructorName(std::string instructor);
    std::string getInstructorName();
    void displayMessage();

private:
    std::string courseName_;
    std::string instructorName_; // instructor's name for the specific course
};

