Distinguish between the terms fatal error and nonfatal error. Why might you prefer to experience a fatal error rather than a nonfatal error?

Unlike nonfatal errors that let the programs run to completion, fatal errors terminate any program without successful completion. In the case of nonfatal errors, our programs may be executed successfully producing wrong results. So experiencing fatal error is preferable as it will alert about wrong code.
