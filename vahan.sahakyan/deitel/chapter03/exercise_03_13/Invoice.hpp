#include <string>

class Invoice
{
public:
    Invoice(std::string number, std::string description, int quantity, int price);

    void setPartNumber(std::string number);
    std::string getPartNumber();

    void setPartDescription(std::string desciption);
    std::string getPartDescription();

    void setItemQuantity(int quantity);
    int getItemQuantity();

    void setPricePerItem(int price);
    int getPricePerItem();

    int getInvoiceAmount();
private:
    std::string partNumber_;
    std::string partDescription_;
    int itemQuantity_;
    int pricePerItem_;
};

