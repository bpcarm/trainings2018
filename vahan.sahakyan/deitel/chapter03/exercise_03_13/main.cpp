#include "Invoice.hpp"
#include <iostream>

int
main()
{
    Invoice invoice("041", "Hammer", 2, 9);

    std::cout << "Part number: " << invoice.getPartNumber() << std::endl;
    std::cout << "Part description: " << invoice.getPartDescription() << std::endl;
    std::cout << "Item quantity: " << invoice.getItemQuantity() << std::endl;
    std::cout << "Price per item: $" << invoice.getPricePerItem() << std::endl;
    std::cout << "Invoice amount: $" << invoice.getInvoiceAmount() << std::endl;

    std::cout << "\nInvoice data members modified.\n\n";
    invoice.setPartNumber("193");
    invoice.setPartDescription("Big nail");
    invoice.setItemQuantity(-15);
    invoice.setPricePerItem(1);

    std::cout << "Part number: " << invoice.getPartNumber() << std::endl;
    std::cout << "Part description: " << invoice.getPartDescription() << std::endl;
    std::cout << "Item quantity: " << invoice.getItemQuantity() << std::endl;
    std::cout << "Price per item: $" << invoice.getPricePerItem() << std::endl;
    std::cout << "Invoice amount: $" << invoice.getInvoiceAmount() << std::endl;
    return 0;
}

