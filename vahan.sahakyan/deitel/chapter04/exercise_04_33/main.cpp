#include <iostream>
#include <unistd.h>

int checkRightTriangle(double hypotenuse, double sideA, double sideB);
int notTriangle();
double getSide(std::string number);
int error1();

int
main()
{
    /// INTRO
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter three side sizes, to check if they are right-angled triangle sides...\n" << std::endl;
    }
    /// input and error check
    double side1 = getSide("1st");
    if (side1 <= 0) return error1();

    double side2 = getSide("2nd");
    if (side2 <= 0) return error1();

    double side3 = getSide("3rd");
    if (side3 <= 0) return error1();

    /// triangle check in general
    if (side1 + side2 <= side3) {
        return notTriangle();
    }
    if (side1 + side3 <= side2) {
        return notTriangle();
    }
    if (side2 + side3 <= side1) {
        return notTriangle();
    }

    /// right-triangle check
    return checkRightTriangle(side3, side1, side2);
    return checkRightTriangle(side2, side1, side3);
    return checkRightTriangle(side1, side2, side3);
}

int
checkRightTriangle(double hypotenuse, double sideA, double sideB)
{
    if (hypotenuse > sideA) {
        if (hypotenuse > sideB) {
            if ((hypotenuse * hypotenuse) == (sideA * sideA) + (sideB * sideB)) {
                std::cout << "\nResult: Correct! These are right-triangle sides!" << std::endl;
                std::cout << "Right-angled sides: " << sideA << " and " << sideB << std::endl;
                std::cout << "Hypotenuse: " << hypotenuse << std::endl;
                return 0;
            }
        }
    }
    std::cout << "\nResult: Wrong! These are NOT right-triangle sides!" << std::endl;
    return 0;
}

int
notTriangle()
{
    std::cout << "\nResult: Wrong! These are NOT even regular triangle sides!" << std::endl;
    return 0;
}

double
getSide(std::string number)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << number << " side: ";
    }
    double side;
    std::cin >> side;
    return side;
}

int
error1()
{
    std::cerr << "\nERROR 1: The sides must be positive.\n";
    return 1;
}

