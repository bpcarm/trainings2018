/// Attempting to use the increment or decrement operator on an expression
/// (other than a modifiable variable name or refference) is a syntax error.

/// It should be written like this:

int sum = x + y;
std::cout << ++sum;

