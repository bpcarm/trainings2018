/// Program that reads an integer and determines and prints whether it is odd or even
#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    int number1; /// declare integer to input by user
    std::cout << "Please, insert an integer: " << std::endl; ///prompt user to input an integer
    std::cin >> number1;

    if (0 == number1 % 2) {
        std::cout << "Your number " << number1 << " is even." << std::endl;
        return 0;
    }
    std::cout << "Your number " << number1 << " is odd." << std::endl;

    return 0; /// indicate that program ended successfully
} /// end of function main

