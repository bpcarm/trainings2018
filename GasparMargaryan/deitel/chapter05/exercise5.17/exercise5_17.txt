i = 1, j = 2, k = 3, m = 2.

1.
std::cout << (i == 1) << std::endl;
prints 1 or true (with std::boolalpha)
parentheses are must

2. 
std::cout << (j == 3) << std::endl;
prints 0 or false (with std::boolalpha)
parentheses are must

3. 
std::cout << (i >= 1 && j < 4) << std::endl;
prints 1 or true (with std::boolalpha)
parentheses are must

4. 
std::cout << (m <= 99 && k < m) << std::endl;
prints 0 or false (with std::boolalpha)
parentheses are must

5. 
std::cout << (j >= i || k == m) << std::endl;
prints 1 or true (with std::boolalpha)
parentheses are must

6. 
std::cout << (k + m < j || 3 - j >= k) << std::endl;
prints 0 or false (with std::boolalpha)
parentheses are must

7. 
std::cout << (!m) << std::endl;
prints 0 or false (with std::boolalpha)
parentheses are not necessary

8. 
cout << (!(j - m)) << endl;
prints 1 or true (with std::boolalpha)
outer parentheses are not necessary, inner are must

9. 
cout << (!(k > m)) << endl;
prints 0 or false (with std::boolalpha)
outer parentheses are not necessary, inner are must

