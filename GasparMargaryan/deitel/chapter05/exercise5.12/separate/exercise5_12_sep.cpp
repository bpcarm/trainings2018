/// Exercise5_12
/// Program that prints patterns
#include <iostream>

int
main()
{
    /// pattern a)
    std::cout << "a) " << std::endl;
    for (int row = 1; row <= 10; ++row) {
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        }
        std::cout << "\n";
    }
    std::cout << "\n";

    /// pattern b)
    std::cout << "b) " << std::endl;
    for (int row = 10; row >= 1; --row) {
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        }
        std::cout << "\n";
    }
    std::cout << "\n";

    /// pattern c)
    std::cout << "c) " << std::endl;
    for (int row = 10; row >= 1; --row) {
        for (int space = 1; space <= 10 - row; ++space) {
            std::cout << " ";
        } 
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        }
        std::cout << "\n";
    }
    std::cout << "\n";

    /// pattern d)
    std::cout << "d) " << std::endl;
    for (int row = 1; row <= 10; ++row) {
        for (int space = 1; space <= 10 - row; ++space) {
            std::cout << " ";
        } 
        for (int column = 1; column <= row; ++column) {
            std::cout << "*";
        }
        std::cout << "\n";
    }
    std::cout << "\n";

    return 0; /// indicate successful termination
} /// end main

