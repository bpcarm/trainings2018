1. 

if ( age >= 65 ) /// no semicolon after if statement
   cout << "Age is greater than or equal to 65 " << endl;
else
   cout << "Age is less than 65 "<< endl; /// closing double colon after 65 not after endl

2. 

if ( age >= 65 )
   cout << "Age is greater than or equal to 65 " << endl;
else  /// no semicolon after else
   cout << "Age is less than 65 "<< endl; /// closing double colon after 65 not after endl

3. 

int x = 1, total = 0;  /// total should be set to 0 (or 1) if we don't initialize it, the variable can contain "garbage" value
while ( x <= 10 )
{
   total += x;
   x++;
}

4. 

while ( x <= 100 )  /// while shoud be typed lowercase
{
   total += x;
   x++;
}  /// should be in block (put inside curely braces)

5. 

while ( y > 0 )
{
   cout << y << endl;
   y--;  /// y should be decremented because if the variable y is positive it should be infinite loop
}
