/// Exercise 4.25
/// A program that prints hollow square by given size
#include <iostream>
#include <unistd.h>

int main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Please enter a number from 1 to 20 to print a hollow square: " << std::endl;
    }  /// prompt user to input value
    int size;  /// declare the size of square
    std::cin >> size;  /// input the size of square
    if (size < 1) {
        std::cout << "Error 1: Invalid number: " << std::endl;  /// end the program with error if inputed value < 1
        return 1;
    }
    if (size > 20) {
        std::cout << "Error 2: Invalid number: " << std::endl;  /// end the program with error if inputed value > 20
        return 2;
    }
    int column = 1; /// declare variable column, set column to 1 as iteration begins
    while (column <= size) { /// loop until row is >= 1
        int row = size; /// initialize the lenghth of row
        while (row >= 1) { /// loop as many times as the size of square is
            if (1 == column) {
                std::cout << "*"; /// output pattern
            } else if (size == column) {
                std::cout << "*"; /// output pattern
            } else if (row == size) {
                std::cout << "*";
            } else if (1 == row) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
            --row; /// decrement row
        } /// end inner while
        ++column; /// increment column
        std::cout << std::endl; // begin new output line
    } /// end outer while

    return 0; /// indicate successful termination
} /// end main

