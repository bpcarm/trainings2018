/// Exercise4_17.cpp
/// Program for determining the winner of salesmen who sold maximum number of units.
#include <iostream>
#include <unistd.h>

int
main()
{
    int counter = 1;
    int largestValue = -2147483648; /// the minimal size of signed integer
    if (::isatty(STDIN_FILENO)) {
        /// prompting user to input the number of sold units
        std::cout << "Enter 10 number of sold units for each salesman !" << std::endl;
    }
    while (counter <= 10) {
        int numberOfUnits;
        std::cin >> numberOfUnits;
        if (numberOfUnits > largestValue) {
            largestValue = numberOfUnits;
        }
        ++counter;
    }
    std::cout << "The winner is the salesman with " << largestValue << " total units sold. " << std::endl;
    /// prints the winner with largest the number of total sold units

    return 0; /// indicate successful termination
} /// end main

